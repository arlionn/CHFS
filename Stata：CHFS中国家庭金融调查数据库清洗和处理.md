
&emsp;

> Stata连享会 &ensp; [主页](https://www.lianxh.cn/news/46917f1076104.html)  || [视频](http://lianxh.duanshu.com) || [推文](https://www.lianxh.cn/news/d4d5cd7220bc7.html) || [知乎](https://www.zhihu.com/people/arlionn/) || [Bilibili 站](https://space.bilibili.com/546535876) 

[![](https://fig-lianxh.oss-cn-shenzhen.aliyuncs.com/lianxhtop00.png)](https://www.lianxh.cn/news/46917f1076104.html)

> **温馨提示：** 定期 [清理浏览器缓存](http://www.xitongzhijia.net/xtjc/20170510/97450.html)，可以获得最佳浏览体验。

> <font color=red>New！</font> **`lianxh` 命令发布了：**    
> 随时搜索推文、Stata 资源。安装：  
> &emsp; `. ssc install lianxh`  
> 详情参见帮助文件 (有惊喜)：   
> &emsp; `. help lianxh`    
> 连享会新命令：`cnssc`, `ihelp`, `rdbalance`, `gitee`, `installpkg`      

&emsp;


> &#x270C; **[课程详情](https://www.lianxh.cn/news/46917f1076104.html)：** <https://gitee.com/lianxh/Course> 

[![](https://fig-lianxh.oss-cn-shenzhen.aliyuncs.com/lianxhtop01.png)](https://www.lianxh.cn/news/46917f1076104.html)

&emsp;

> &#x26F3; **[课程主页](https://www.lianxh.cn/news/46917f1076104.html)：** <https://gitee.com/lianxh/Course>

[![](https://fig-lianxh.oss-cn-shenzhen.aliyuncs.com/lianxhtop02.png)](https://www.lianxh.cn/news/46917f1076104.html)

&emsp;

> &#x26F3; Stata 系列推文：

- [全部](https://www.lianxh.cn/blogs.html)  | [Stata入门](https://www.lianxh.cn/blogs/16.html) |  [Stata教程](https://www.lianxh.cn/blogs/17.html) |  [Stata资源](https://www.lianxh.cn/blogs/35.html) | [Stata命令](https://www.lianxh.cn/blogs/43.html) 
- [计量专题](https://www.lianxh.cn/blogs/18.html) | [论文写作](https://www.lianxh.cn/blogs/31.html) | [数据分享](https://www.lianxh.cn/blogs/34.html) |  [专题课程](https://www.lianxh.cn/blogs/44.html)
- [结果输出](https://www.lianxh.cn/blogs/22.html) | [Stata绘图](https://www.lianxh.cn/blogs/24.html) | [数据处理](https://www.lianxh.cn/blogs/25.html) |  [Stata程序](https://www.lianxh.cn/blogs/26.html)
- [回归分析](https://www.lianxh.cn/blogs/32.html) |  [面板数据](https://www.lianxh.cn/blogs/20.html)  | [交乘项-调节](https://www.lianxh.cn/blogs/21.html)  | [IV-GMM](https://www.lianxh.cn/blogs/38.html) 
- [内生性-因果推断](https://www.lianxh.cn/blogs/19.html) |  [倍分法DID](https://www.lianxh.cn/blogs/39.html) |  [断点回归RDD](https://www.lianxh.cn/blogs/40.html) |  [PSM-Matching](https://www.lianxh.cn/blogs/41.html) |  [合成控制法](https://www.lianxh.cn/blogs/42.html)
- [Probit-Logit](https://www.lianxh.cn/blogs/27.html) |  [时间序列](https://www.lianxh.cn/blogs/28.html) |  [空间计量](https://www.lianxh.cn/blogs/29.html) | [分位数回归](https://www.lianxh.cn/blogs/48.html) | [生存分析](https://www.lianxh.cn/blogs/46.html) | [SFA-DEA](https://www.lianxh.cn/blogs/49.html)
- [文本分析-爬虫](https://www.lianxh.cn/blogs/36.html) |  [Python-R-Matlab](https://www.lianxh.cn/blogs/37.html) | [机器学习](https://www.lianxh.cn/blogs/47.html)
- [Markdown](https://www.lianxh.cn/blogs/30.html)  | [工具软件](https://www.lianxh.cn/blogs/23.html) |  [其它](https://www.lianxh.cn/blogs/33.html)


> &#x261D; [PDF下载 - 推文合集](https://www.jianguoyun.com/p/DXgO9zoQtKiFCBj6tPED )

&emsp;




&emsp;

> **作者：** 李青塬 (广东工业大学)    
> **邮箱：** <qingyuanli95@gmail.com>


&emsp;

---

**目录**
[[TOC]]

---

&emsp;


## 1. 引言

对于实证研究者而言，数据是研究的重要原材料。巧妇难为无米之炊，通常很多同学写毕业论文喜欢用问卷调查数据，一般都是用发朋友圈求填问卷的方法来搜集数据。不过，这种方法存在较大缺陷，在答辩时，通常会引起答辩评委的质疑。其实，有很多免费的微观数据可以供研究者使用，这些数据包括中国健康与养老追踪调查数据( CHARLS ) 、中国家庭追踪调查数据 ( CFPS ) 以及中国家庭金融调查数据 ( CHFS )等等。一方面，这些微观数据库有很强的权威性和科学性，另一方面，数据内容十分丰富，也足以对社科领域的许多题目进行研究。

往往，使用这些数据有些许门槛，刚开始拿到数据手足无措。那么，我们该怎么处理这些微观数据呢？本文以西南财经大学的中国家庭金融调查 ( CHFS )为例，尝试介绍该微观数据库，并进行一些描述性分析。

&emsp;

## 2. 文档管理

通常，我们在处理数据的过程中会产生许多文件，让每个文件都找到自己的“去处”十分重要。或许我们可以根据自己研究的论文题目来组织文档，一个题目可以创建一个文档库。文档库中包含以下九个子文件夹。(关于文件夹的设定，参见 []())
* Paper: 论文所有版本
* References：所参考的文献
* Raw_data ：原始数据，只进行读取，不写入
* Working_data ：存放对原数据进行处理后的数据
* Temp_data：存放临时数据，当程序完成后即可删除
* Dofiles：存放 Stata 的代码文件
* Logfiles：存放 Stata 的日志文件
* Figures：存放生成的图片
* Tables：存放生成的图表

手动创建 9 个文件夹有点不便，于是我编写了以下代码，能够实现自动创建文档库。您可以直接在以下的 do 文件中修改路径，即可自动创建研究文档库。

```Stata
-------------------------------------------------
      name:  <unnamed>
       log:  E:\CHFS\Logfiles\Stata连享会_CHFS数据处理.log
  log type:  text
 opened on:   3 Jun 2021, 21:12:03

/*===========================================
* Program: Stata连享会_CHFS数据处理
* Data:    CHFS 2015 2017 
* Aim:     Have fun with Stata 
* Revised: 06/15/2021
* =========================================== */
. clear

// 在 E 盘自动创建 CHFS 研究题目，
// 并在其下创建 9 个子文件夹，可以直接修改路径创建研究话题
. global root= "E:\CHFS"       
. global dofiles= "$root\Dofiles"
. global logfiles= "$root\Logfiles"           
. global raw_data= "$root\Raw_data"
. global working_data= "$root\Working_data"
. global temp_data= "$root\Temp_data"
. global tables= "$root\Tables"
. global figures= "$root\Figures"

. cap !mkdir "$raw_data"              
. cap  mkdir "$temp_data"        // 自动创建文件夹
. cap  mkdir "$working_data"
. cap  mkdir "$tables"           // `cap` 命令可让错误的代码继续运行
. cap  mkdir "$figures"
. cap  mkdir "$dofiles"          // 如果已经创建了这些文件夹，也可以运行
. cap  mkdir "$logfiles"
. cap  mkdir "$root\Paper"
. cap  mkdir "$root\References"
. cap  mkdir "$raw_data"
. cap  mkdir "$working_data"
. cap  mkdir "$figures"          // 以上命令在每次打开 do 文件都可运行
```
&emsp;

## 3. 获取数据

首先，数据是免费的。但是，我们需要从官方渠道申请数据。可以从 CHFS 网站进行数据下载，该平台提供 Stata 数据格式，并定期更新数据。打开 CHFS 官网后，在首页右上角点击数据申请入口即可注册并进行认证，认证速度很快，一般能在短时间内通过。之后，即可下载所需的数据。我们可以下载 2015 年和 2017 年共 2 年的数据。 数据拿到了，应该放在哪里呢？下载的原始数据就可以放在刚刚创建的 Raw_data 中。从官网下载的是一个压缩包，可以解压在当前文件夹内。

- CHFS 数据中心网址：

- <https://chfs.swufe.edu.cn/>


&emsp;

## 4. 数据管理

在对数据进行分析时，我们经常会遇到这些事情：合并多个文件；删除某个变量；重新生成一个新变量；计算某个变量的函数值等。这些事情的处理就是数据管理。本文试着介绍一些常用的数据管理命令，值得注意的是，以下数据处理及描述仅为了理解方便，具体论文的数据处理还需具体分析。

### 4.1 数据导入

CHFS 的数据的导入至少有两种方法。一种是直接拖进软件中，因为 CHFS 数据的格式是 dta 的格式，可以直接拖动数据到 Stata 中。另外一种是使用代码，设定路径来导入。在导入数据前，通常可对结果窗口进行“录像”，用 log 来保存。
```Stata

. /* use,导入原始数据 */ 
. set matsize 5000

. set more off

. use "$raw_data\CHFS数据-2015\2015年中国家庭金融调查数据dta格式-stata14以上版本\chfs2015_hh_20191120_version14.dta", clear
```
### 4.2 数据浏览及变量定义

数据导入后，我们可以从宏观层面上概览一下整体的数据情况。运用 `describe` 命令可以对数据库中的所有变量及其标签进行描述。此外，还可以打开数据窗口进行浏览和编辑。一种方式是手动打开，另一种就是运用命令打开。

```Stata

. describe   //描述所有数据  

. browse     //浏览数据

. edit       //编辑数据 
```
恢复数据可以用 preserve 和 restore 两个命令。preserve 可以保存数据，保证程序终止后数据恢复。 restore 强制恢复数据。值得注意的是，在命令运行时 preserve 和 restore 不能中断。对于变量名的改变，我们可以运用 `rename` 命令来操作。以下的命令展示了对变量名的批量大小写改写以及前后缀变换。

```Stata

. preserve              //与 restore 一起使用可以恢复数据
. drop _all                             //删除所有数据
. restore

. preserve 
. rename _all, upper    //所有变量大写
. rename _all, lower    //所有变量小写
. rename _all, proper   //所有变量名首字母大写
. rename * *_2021       //所有变量名后加相同后缀前缀
. rename (*_2021 ) (havefun_*)  //批量修改变量名的前后缀
. restore
```
我们还可以为变量添加标签，也可以对观测值和变量进行排序：
```Stata

. gen family_size=a2000a+a2000b
(21,791 missing values generated)

. label var family_size "家庭规模"    //为变量增加标签

. sort  family_size                  //对数据进行排序

. order  hhid family_size            //对变量进行排序
```

### 4.3 删除生成及替代变量

有时候，我们需要对一个变量进行异常值处理，最常见的异常值就是重复数据。可以利用 duplicates 进行查询和删除：
```Stata

. duplicates list hhid       //查询有无重复家户
Duplicates in terms of hhid
(0 observations are duplicates)

. duplicates drop hhid,force    //删除重复家户
Duplicates in terms of hhid
(0 observations are duplicates)
```
在进行实证研究前，需要许多变量，而 CHFS 中的原始数据可能不能直接使用，需要我们做初步处理。以下是一些处理方法。其中， encode 可以把字符串格式转换为数值型格式。 recode 可以对变量的值进行重新编排。
```Stata

. encode hhid, gen(newhhid)                                 //变量格式转换

. gen nor=1 if d1101==1 | d2101==1 
(10,682 missing values generated)

. recode nor(1=1)(.=0),gen(no_risk)        //变量值替换并产生新变量，无风险资产
(10682 differences between nor and no_risk)

. recode a4011c (1=5)(2=4)(3=3)(4=2)(5=1),gen(happiness)   //产生幸福感变量
(25272 differences between a4011c and happiness)
. recode c1001 (1=1)(2=2)(3=2) ,gen(hous)       
(772 differences between c1001 and hous)

. gen nf=c7001+hous
(29 missing values generated)

. recode nf(2/3=1)(4=0),gen(no_fin)                         //非金融资产
(21720 differences between nf and no_fin)

. recode hous (1=1)(2=0) ,gen(house)                            //房产
(2602 differences between hous and house)

. recode c7001(1=1)(2=0),gen(car)                               //汽车
(16931 differences between c7001 and car)
```

如果要形成面板数据，那么时间变量必不可少。我们可用 gen 生成一个时间变量。对于可以进行数值运算的一些变量，倘若要进行平均数，最大值和中位数等处理，就可以用 egen 来产生。运用 replace 也可在原始的观测值上进行变化。
```Stata

. gen year=2015                           //产生新变量，生成年份 

. egen incomemedian=median(total_income)  //产生新变量，去年总收入的中位数

. egen incomemax=max(total_income)        //产生新变量，去年总收入的最大值

. replace incomemax=incomemax/10          //把 incomemax 的值缩小 10 倍
(37,243 real changes made)
```
假设研究不同幸福感水平下的收入情况，我们可以进行分组函数计算在不同幸福感水平上的总收入均值、总收入累计值和同等幸福感水平上的最大值或最小值：
```Stata
. bys happiness: egen mean_tolincome=mean(total_income)       //分组求平均  

. bys happiness: egen newvar=sum(total_income)          //计算分组后的累计和  

. bys happiness year: egen maxvar=max(total_income)      //计算分组后求最大或最小值

```

CHFS 数据库中的大多数变量对于研究者而言是无用的。这就需要我们做出一个积极的决定，只保留所需变量或观测值，所谓“断舍离”。保留所需的变量可以提高效率，保留所需的观测值可以提高研究的针对性。这时，我们主要使用 keep 和 drop 两个命令。
```Stata

. drop if happiness==. | happiness==.d | happiness==.r     //删除无效值
(46 observations deleted)

. drop if track==0                                        //保留追访家户
(15,494 observations deleted)

. keep if  track!=0                                       //保留追访家户
(0 observations deleted)

. keep hhid  family_size   year  happiness total_income   //保留需要研究的变量

. save $working_data\chfs_2015_hh.dta, replace            //保存数据
file E:\CHFS\Working_data\chfs_2015_hh.dta saved
```

### 4.4 数据文件的合并

CHFS 有三个数据库，各自包含一些信息，研究通常的变量可能分布在不同的数据库中。同时，也有可能从外部得到一些数据，比如省级层面的宏观经济数据，天气情况等等。对于不同年份的数据，我们想做成面板数据，还需要进行跨年合并。这些情况下我们就主要运用 append 和 merge 两个命令：
```Stata
. *相同变量数据文件合并，即跨年的面板数据合并
. use $raw_data\CHFS数据-2017\CHFS2017年调查数据-stata14版本\chfs2017_hh_202104.dta, clear 

. rename a1111 family_size  

. ren b2003b total_income 

. recode h3514 (1=5)(2=4)(3=3)(4=2)(5=1),gen(happiness)   //产生幸福感变量
(30010 differences between h3514 and happiness)

. gen year=2017

. keep hhid    family_size   year  total_income happiness //保留需要研究的变量

. append using $working_data\chfs_2015_hh.dta       //使用 append 让相同变量数据文件合并
(note: variable family_size was byte, now float to accommodate using data's values)
(note: variable total_income was long, now double to accommodate using data's values)

. save $working_data\chfs_2015_2017_hh.dta, replace       //保存数据
file E:\CHFS\Working_data\chfs_2015_2017_hh.dta saved
```

刚刚完成了 2015 年和 2017 年家庭层面数据的合并，形成了面板数据。往往在研究中，需要在原数据中加入新的变量进行合并，这时就需要使用 merge 这个命令，以下是 CHFS 2015 年 hh 数据与 master 数据的合并。在实际论文数据处理中，我们可能需要进行大量的数据合并，还需要合并个人数据以及外部数据等，本文此处的合并仅作参考。
```Stata

. *在原数据中加入新变量合并
. /* use,导入原始数据 */ 
. use $raw_data\CHFS数据-2015\2015年中国家庭金融调查数据dta格式-stata14以上版本\chfs2015_master_city_20180504_version14, clear 

. /* merge,加入新变量合并数据 */ 
. merge m:1 hhid using $working_data\chfs_2015_hh.dta  //合并hh数据和master数据

    Result                           # of obs.
    -----------------------------------------
    not matched                        15,540
        from master                    15,540  (_merge==1)
        from using                          0  (_merge==2)

    matched                            21,749  (_merge==3)
    -----------------------------------------

. keep if _merge==3            //保留匹配后所需数据
(15,540 observations deleted)

. drop _merge                  //需删除，若后续再次匹配则还会生成该变量

. save $working_data\chfs_hh_master_2015_hh.dta, replace  //保存数据
file E:\CHFS\Working_data\chfs_hh_master_2015_hh.dta saved
```

&emsp; 

## 5. 描述性统计

经济学的经验分析通常分为描述性分析和回归分析。描述性分析通常考察变量的分布特征，计算集中趋势和离散情况。通常有两种作用：其一是在统计推断或者因果分析之前，发现、界定或者详细讨论要研究的问题；另一个是在一定程度上验证假说。回归分析才是计量经济学的核心内容。在进行回归分析前，通过对数据的描述，可以在很大程度上了解数据的脾性。本节使用 CHFS 2015 的个人数据进行描述性统计。

### 5.1 单变量描述性统计

单变量的描述性统计分析是指单个变量的集中趋势和离散趋势进行分析。变量即变化的量，变量的测量值就是观测值，一个或多个变量及其观测值就构成了数据。了解变量和数据是进行计量经济分析的初步工作。
```Stata
. use $raw_data\CHFS数据-2015\2015年中国家庭金融调查数据dta格式-stata14以上版本\chfs2015_ind_20191120_version14.dta,clear  //导入数据

. recode a2003 (1=1)(2=0),gen(gender)         //性别
(65568 differences between a2003 and gender)

. gen age=2021-a2005                          //年龄
(159 missing values generated)

. rename a2012 edu                            //受教育程度

. recode a3000(1=1)(2=0),gen(work)            //有无工作
(41902 differences between a3000 and work)

. rename a2025b health                        //健康程度

. rename a2024 marital                        //婚姻状况

. gen income=a3020+a3022+a3023-a3024          //收入
(106,722 missing values generated)

. recode a3014a(1/4=1)(5/7=0)(8=1)(9/10=0),gen(department)  //是否国企
(33385 differences between a3014a and department)
```

频次分析是最基本的描述统计分析，衡量类别的集中趋势，我们可以用 tabulate(tab) 命令先对性别进行分析。描述的结果中，第一列是变量的取值，第二列的频次，第三列是百分比，第四列是累积百分比。结果显示，总体样本中，男性占比达到 50.7% 。
```Stata
. tab gender,m            //性别分布

  RECODE of |
      a2003 |
    (（新样 |
     本）家 |
     庭成员 |
      性别) |      Freq.     Percent        Cum.
------------+-----------------------------------
          0 |     65,568       49.23       49.23
          1 |     67,523       50.70       99.93
          . |         92        0.07      100.00
------------+-----------------------------------
      Total |    133,183      100.00
```
下表是教育程度分布与描述，结果显示高中及高中以下教育水平的个体占了 65.28% ，大学本科以上教育程度大约占比 8% 。在 `tab` 命令后加 d 可以显示更加详细的描述性统计结果，总体均值为初高中教育程度。
```Stata

. tab edu,m               //教育程度分布

     家庭成 |
     员的文 |
     化程度 |      Freq.     Percent        Cum.
------------+-----------------------------------
          1 |     12,559        9.43        9.43
          2 |     23,778       17.85       27.28
          3 |     34,645       26.01       53.30
          4 |     15,958       11.98       65.28
          5 |      6,376        4.79       70.07
          6 |      8,460        6.35       76.42
          7 |     10,149        7.62       84.04
          8 |        898        0.67       84.71
          9 |        158        0.12       84.83
          . |     20,202       15.17      100.00
------------+-----------------------------------
      Total |    133,183      100.00

. sum edu,d                 //平均值与方差

                 家庭成员的文化程度
-------------------------------------------------------------
      Percentiles      Smallest
 1%            1              1
 5%            1              1
10%            1              1       Obs             112,981
25%            2              1       Sum of Wgt.     112,981

50%            3                      Mean           3.453421
                        Largest       Std. Dev.      1.782346
75%            4              9
90%            6              9       Variance       3.176758
95%            7              9       Skewness       .7320712
99%            7              9       Kurtosis       2.714751
```

我们可以用 sum 和 if 进行分样本进行描述。下表显示了在不同性别情况下，男性的受教育水平均值要大于女性。
```Stata

. sum edu if gender==1           //男性受教育程度

    Variable |        Obs        Mean    Std. Dev.       Min        Max
-------------+---------------------------------------------------------
         edu |     56,572    3.621456    1.731099          1          9

. sum edu if gender==0           //女性受教育程度

    Variable |        Obs        Mean    Std. Dev.       Min        Max
-------------+---------------------------------------------------------
         edu |     56,357    3.284579    1.816566          1          9
```


运用 `centile` 命令可以计算分位数。结果的第三列表示分位数，第四列表示相应的分位数的值。最后两列是 95% 的置信区间。

```Stata
. centile edu if  gender==1  ,centile(10(10)90)  //计算男性样本中 10% ， 20% ，...， 90% 分位数

                                                       -- Binom. Interp. --
    Variable |       Obs  Percentile    Centile        [95% Conf. Interval]
-------------+-------------------------------------------------------------
         edu |    56,572         10           2               2           2
             |                   20           2               2           2
             |                   30           3               3           3
             |                   40           3               3           3
             |                   50           3               3           3
             |                   60           3               3           4
             |                   70           4               4           4
             |                   80           5               5           5
             |                   90           7               7           7
```

### 5.2 （定性—定性）变量描述性统计

在进行经济现象的研究中，我们对两个变量或两个以上的变量之间的关系更感兴趣。（定性—定性）变量的描述统计分析就是当两个或两个以上的变量均为定性变量时，应用描述统计分析方法考察两个变量的关系。我们可以用 `tab` 命令，添加 `row` 选项可以显示百分比，添加 `chi2` 命令可以进行卡方检验。下表考察不同性别的受教育程度分布差异及并卡方检。从结果中可以看出，在初中教育水平上（文化程度为 3 以上）的占比中，男性一直高于女性。卡方检验也表明两类个体具有显著差异。

```Stata
. *定性——定性变量描述统计
. tab gender edu,row chi2             //不同性别的受教育程度分布差异，并卡方检验

+----------------+
| Key            |
|----------------|
|   frequency    |
| row percentage |
+----------------+

RECODE of |
  a2003 |
(（新样 |
 本）家 |
 庭成员 |                             家庭成员的文化程度
  性别) |      1       2       3       4       5        6        7       8      9 |   Total
--------+--------------------------------------------------------+-------------------------
      0 |  8,944  12,401  15,660   7,176   3,052    3,966    4,690     419     49 |  56,357 
        |  15.87   22.00   27.79   12.73    5.42     7.04     8.32    0.74   0.09 |  100.00 
--------+--------------------------------------------------------+-------------------------
      1 |  3,606  11,371  18,969   8,779   3,316    4,490    5,454     479    108 |  56,572 
        |   6.37   20.10   33.53   15.52    5.86     7.94     9.64    0.85   0.19 |  100.00 
--------+--------------------------------------------------------+-------------------------
  Total | 12,550  23,772  34,629  15,955   6,368    8,456   10,144     898    157 | 112,929 
        |  11.11   21.05   30.66   14.13    5.64     7.49     8.98    0.80   0.14 |  100.00 
       Pearson chi2(8) =  2.9e+03   Pr = 0.000
```


我们还可以进行更加多维度的频次分析。可以考察不同性别、教育程度在工作单位性质的区别。总体而言，无论性别如何，受教育程度越高，进入国企的比率越大。
```Stata

. by gender, sort:tab edu department,row   //不同性别、教育程度在工作单位性质的区别

-----------------------------------------------------
-> gender = 0

+----------------+
| Key            |
|----------------|
|   frequency    |
| row percentage |
+----------------+

           |   RECODE of a3014a
    家庭成 |     (家庭成员工作
    员的文 |      单位的类型)
    化程度 |         0          1 |     Total
-----------+----------------------+----------
         1 |       365         40 |       405 
           |     90.12       9.88 |    100.00 
-----------+----------------------+----------
         2 |     1,525        180 |     1,705 
           |     89.44      10.56 |    100.00 
-----------+----------------------+----------
         3 |     3,252        660 |     3,912 
           |     83.13      16.87 |    100.00 
-----------+----------------------+----------
         4 |     1,244        530 |     1,774 
           |     70.12      29.88 |    100.00 
-----------+----------------------+----------
         5 |       737        459 |     1,196 
           |     61.62      38.38 |    100.00 
-----------+----------------------+----------
         6 |     1,116      1,045 |     2,161 
           |     51.64      48.36 |    100.00 
-----------+----------------------+----------
         7 |     1,059      1,606 |     2,665 
           |     39.74      60.26 |    100.00 
-----------+----------------------+----------
         8 |        86        201 |       287 
           |     29.97      70.03 |    100.00 
-----------+----------------------+----------
         9 |         6         27 |        33 
           |     18.18      81.82 |    100.00 
-----------+----------------------+----------
     Total |     9,390      4,748 |    14,138 
           |     66.42      33.58 |    100.00 


-----------------------------------------------------
-> gender = 1

+----------------+
| Key            |
|----------------|
|   frequency    |
| row percentage |
+----------------+

           |   RECODE of a3014a
    家庭成 |     (家庭成员工作
    员的文 |      单位的类型)
    化程度 |         0          1 |     Total
-----------+----------------------+----------
         1 |       294         37 |       331 
           |     88.82      11.18 |    100.00 
-----------+----------------------+----------
         2 |     2,274        276 |     2,550 
           |     89.18      10.82 |    100.00 
-----------+----------------------+----------
         3 |     5,901      1,270 |     7,171 
           |     82.29      17.71 |    100.00 
-----------+----------------------+----------
         4 |     2,133      1,217 |     3,350 
           |     63.67      36.33 |    100.00 
-----------+----------------------+----------
         5 |       985        654 |     1,639 
           |     60.10      39.90 |    100.00 
-----------+----------------------+----------
         6 |     1,235      1,408 |     2,643 
           |     46.73      53.27 |    100.00 
-----------+----------------------+----------
         7 |     1,173      1,901 |     3,074 
           |     38.16      61.84 |    100.00 
-----------+----------------------+----------
         8 |        84        235 |       319 
           |     26.33      73.67 |    100.00 
-----------+----------------------+----------
         9 |         8         70 |        78 
           |     10.26      89.74 |    100.00 
-----------+----------------------+----------
     Total |    14,087      7,068 |    21,155 
           |     66.59      33.41 |    100.00 
```

### 5.3 （定性—定量）变量描述性统计

当一个变量是定性变量，一个变量为定量变量时，考察二者的关系就是（定性—定量）描述性统计分析。具体而言，即分组变量是定性变量时，要描述的变量是定量变量。我们可以用 `tabstat` 命令，通常需要加两个选项，一个是 by（）选项，界定分组变量；另一个是 statistics （）可以加上需要报告的统计量。例如 mean （平均值）、p50 （中位数）等。下表考察了不同性别的收入、标准差、中位数、最大值和最小值。可以看出男性的平均收入要高于女性，标准差也较女性高。

```Stata

. *定性——定量变量描述统计
. tabstat income,by(gender) statistics(mean sd p50 max min)   //不同性别收入

Summary for variables: income
     by categories of: gender (RECODE of a2003 (（新样本）家庭成员性别))

  gender |      mean        sd       p50       max       min
---------+--------------------------------------------------
       0 |  26641.69  22954.32     22000  209999.9 -8000.007
       1 |  32493.44  27502.74  26597.04  229999.9    -20000
---------+--------------------------------------------------
   Total |  30119.07  25913.57     24000  229999.9    -20000
------------------------------------------------------------
```


下表表示不同教育程度的平均收入差异。结果显示，随着教育程度提高，平均收入也相应提高。但收入差异（标准差）在硕士学历中最大（教育程度为 8 的个体中）。
```Stata
. tabstat income,by(edu) statistics(mean sd)           //不同教育程度的平均收入

Summary for variables: income
     by categories of: edu (家庭成员的文化程度)

     edu |      mean        sd
---------+--------------------
       1 |  16622.82   14846.1
       2 |  22053.84  17984.86
       3 |  25752.82  19361.01
       4 |  28169.85  21516.89
       5 |  29786.09  23356.15
       6 |  36311.05  27473.99
       7 |  47925.91  37661.57
       8 |  69558.22  49129.55
       9 |  85211.61  44959.43
---------+--------------------
   Total |  30185.23  25950.62
------------------------------

. log close                                //关闭 log 文件
      name:  <unnamed>
       log:  E:\CHFS\Logfiles\Stata连享会_CHFS数据处理.log
  log type:  text
 closed on:   3 Jun 2021, 21:12:49
 --------------------------------------------------------------
```

&emsp;

## 6. 本文小结

本文首先从研究的原材料出发，推荐使用 CHARLS 、CFPS 等大型微观调查数据库，并以 CHFS 为例介绍数据的获取方法。其次，介绍了文档管理方法，并编写了自动创建学术研究文档库的 do 文件。再次，本文利用 CHFS 数据尝试对数据导入、数据保存以及面板数据合并等数据管理方法进行简要介绍。最后，结合具体实例，分别对单变量和多变量进行描述性分析。需要提醒的是，笔者作为 Stata 初学者，本文的处理方法仅作参考，具体论文的数据处理还需具体分析。

&emsp; 

## 7. 附：CHFS_data_dofile.do

最后附上本文的代码，供大家参考： 
```Stata
/* log,记录结果窗口 */
cap log close
log using $logfiles\Stata连享会_CHFS数据清洗.log, replace
/* ======================================================
 * Program: Stata连享会_CHFS数据处理
 * Data:	CHFS 2015 2017 
 * Aim:   	Have fun with Stata 
 * Revised: 06/03/2021
 ======================================================== */

clear
global root= "E:\CHFS"       //在 E 盘自动创建 CHFS 研究题目，并在其下创建 9 个子文件夹，可以直接修改路径创建研究话题
global dofiles= "$root\Dofiles"
global logfiles= "$root\Logfiles"           
global raw_data= "$root\Raw_data"
global working_data= "$root\Working_data"
global temp_data= "$root\Temp_data"
global tables= "$root\Tables"
global figures= "$root\Figures"
cap !mkdir "$raw_data"              
cap mkdir "$temp_data"                //自动创建文件夹
cap mkdir "$working_data"
cap mkdir "$tables"                   // `cap` 命令可让错误的代码继续运行
cap mkdir "$figures"
cap mkdir "$dofiles"                  //如果已经创建了这些文件夹，也可以运行
cap mkdir "$logfiles"
cap mkdir "$root\Paper"
cap mkdir "$root\References"
cap mkdir "$raw_data"
cap mkdir  "$working_data"
cap mkdir "$figures"                     //以上命令在每次打开 do 文件都可运行
 
/* use,导入原始数据 */ 
set matsize 5000
set more off
use $raw_data\CHFS数据-2015\2015年中国家庭金融调查数据dta格式-stata14以上版本\chfs2015_hh_20191120_version14.dta,clear

describe   //描述所有数据
browse              //浏览数据
edit       //编辑数据 
preserve               //与restore一起使用可以恢复数据
drop _all
restore
preserve
rename _all,upper     //所有变量大写
rename _all,lower     //所有变量小写
rename _all,proper    //所有变量名首字母大写
rename * *_2021         //所有变量名后加相同后缀前缀
rename (*_2021 ) (havefun_*)  //批量修改变量名的前后缀
restore
gen family_size=a2000a+a2000b
label var family_size "家庭规模"            //为变量增加标签
sort  family_size                     //对数据进行排序
order  hhid family_size            //对变量进行排序

duplicates list hhid    //查询有无重复家户
duplicates drop hhid,force   //删除重复家户
gen year=2015           //产生新变量，生成年份 
egen incomemedian=median(total_income)  //产生新变量，去年总收入的中位数
egen incomemax=max(total_income)   //产生新变量，去年总收入的最大值
encode hhid, gen(newhhid)      //变量格式转换
gen nor=1 if d1101==1 | d2101==1 
recode nor(1=1)(.=0),gen(no_risk)   //变量值替换并产生新变量，无风险资产
recode a4011c (1=5)(2=4)(3=3)(4=2)(5=1),gen(happiness)   //产生幸福感变量
drop if happiness==. | happiness==.d | happiness==.r  //删除无效值
replace incomemax=incomemax/10       //把 incomemax 的值缩小 10 倍
drop if track==0                       //保留追访家户
keep if  track!=0                    //保留追访家户
bys hhid: egen mean_tolincome=mean(total_income)     //分组求平均  
bys hhid: egen newvar=sum(total_income)     //计算分组后的累计和  
bys hhid year: egen cc=max(total_income)      //计算分组后求最大最小值
recode c1001 (1=1)(2=2)(3=2) ,gen(hous)       
gen nf=c7001+hous
recode nf(2/3=1)(4=0),gen(no_fin)  //非金融资产
recode hous (1=1)(2=0) ,gen(house)  //房产
recode c7001(1=1)(2=0),gen(car)  //汽车
keep hhid  family_size   year  happiness total_income     //保留需要研究的变量
save $working_data\chfs_2015_hh.dta, replace    //保存数据

*相同变量数据文件合并，即跨年的面板数据合并
use $raw_data\CHFS数据-2017\CHFS2017年调查数据-stata14版本\chfs2017_hh_202104.dta, clear 
rename a1111 family_size  
ren b2003b total_income 
recode h3514 (1=5)(2=4)(3=3)(4=2)(5=1),gen(happiness)   //产生幸福感变量
gen year=2017
keep hhid    family_size   year  total_income happiness  //保留需要研究的变量
append using $working_data\chfs_2015_hh.dta   //相同变量数据文件合并
save $working_data\chfs_2015_2017_hh.dta, replace    //保存数据

*在原数据中加入新变量合并
/* use,导入原始数据 */ 
use $raw_data\CHFS数据-2015\2015年中国家庭金融调查数据dta格式-stata14以上版本\chfs2015_master_city_20180504_version14, clear 
/* merge,加入新变量合并数据 */ 
merge m:1 hhid using $working_data\chfs_2015_hh.dta //合并hh数据和master数据
keep if _merge==3                                     //保留匹配后所需数据
drop _merge                                         //需删除，若后续再次匹配则还会生成该变量
save $working_data\chfs_hh_master_2015_hh.dta, replace    //保存数据

use $raw_data\CHFS数据-2015\2015年中国家庭金融调查数据dta格式-stata14以上版本\chfs2015_ind_20191120_version14.dta,clear  //导入数据
recode a2003 (1=1)(2=0),gen(gender)        //性别
gen age=2021-a2005                         //年龄
rename a2012 edu                           //教育程度
recode a3000(1=1)(2=0),gen(work)           //有无工作
rename a2025b health           //健康程度
rename a2024 marital            //婚姻状况
gen income=a3020+a3022+a3023-a3024    //收入
recode a3014a(1/4=1)(5/7=0)(8=1)(9/10=0),gen(department)  //是否国企
tab gender,m            //性别分布
tab edu,m               //教育程度分布
sum edu,d               //平均值与方差
sum edu if gender==1    //男性受教育程度
sum edu if gender==0    //女性受教育程度
centile edu if  gender==1  ,centile(10(10)90)  //计算男性样本中 10% ， 20% ，...， 90% 分位数

*定性——定性变量描述统计
drop if gender==.
tab gender edu,row chi2             //不同性别的受教育程度分布差异，并卡方检验
by gender,sort:tab edu department,row   //不同性别、教育程度在工作单位性质的区别

*定性——定量变量描述统计
tabstat income,by(gender) statistics(mean sd p50 max min)   //不同性别收入
tabstat income,by(edu) statistics(mean sd)  //不同教育程度的平均收入
log close                //关闭 log 文件
```


&emsp;

## 7. 参考文献

- 《计量经济学实验教程》刘泽云 孙志军. [-Link-](https://item.jd.com/12482413.html)

 &emsp;


## 8. 相关推文

> Note：产生如下推文列表的 Stata 命令为：   
> &emsp; `lianxh 数据清洗 数据处理`  
> 安装最新版 `lianxh` 命令：    
> &emsp; `ssc install lianxh, replace` 

- 专题：[专题课程](https://www.lianxh.cn/blogs/44.html)
  - [直播课：Stata 数据清洗之实战操作（二）](https://www.lianxh.cn/news/57869f51cf328.html)
  - [⏫ 连享会直播：Stata 数据清洗之实战操作](https://www.lianxh.cn/news/f785de82434c1.html)
- 专题：[Stata教程](https://www.lianxh.cn/blogs/17.html)
  - [普林斯顿Stata教程(一) - Stata数据处理](https://www.lianxh.cn/news/c98db865ea7fb.html)
- 专题：[数据处理](https://www.lianxh.cn/blogs/25.html)
  - [Stata数据处理：清洗CFPS数据库](https://www.lianxh.cn/news/2916ae8363459.html)
  - [Stata数据处理：通过API获取经济数据](https://www.lianxh.cn/news/5fc08c3046716.html)
  - [Stata数据处理：超大Excel文档如何读入](https://www.lianxh.cn/news/798d66e2c0acc.html)
  - [滚动吧统计量！Stata数据处理](https://www.lianxh.cn/news/08bfff06551c4.html)
  - [Stata数据处理：各种求和方式一览](https://www.lianxh.cn/news/3ce33ba6750a7.html)
  - [Stata数据处理：字符型日期变量的转换](https://www.lianxh.cn/news/e19cec1139a11.html)
  - [Stata数据处理：赫芬达尔指数-(hhi5)-命令介绍](https://www.lianxh.cn/news/b426fbef84f81.html)
  - [Stata数据处理：统计组内非重复值个数](https://www.lianxh.cn/news/3f5d25925cd54.html)
  - [Stata数据处理：分年度-行业计算销售额前四名的行业占比](https://www.lianxh.cn/news/c51bd14bb6ba6.html)
  - [Stata数据处理：物价指数-(CPI)-的导入和转换](https://www.lianxh.cn/news/c7eb4207e1d7d.html)
  - [Stata数据处理：快速读取万德-Wind-数据-readWind2](https://www.lianxh.cn/news/53ceeb15af331.html)
  - [Stata数据处理：用-efolder-快速生成文件夹和子文件夹](https://www.lianxh.cn/news/5b0643f903c09.html)
  - [Stata数据处理：用-astile-快速创建分组](https://www.lianxh.cn/news/0a2a332eac456.html)
  - [Stata数据处理：FRED数据导入问题的解决方案](https://www.lianxh.cn/news/b7b1ec1c5b9c2.html)
  - [Stata数据处理：ftree命令-用txt文档记录文件夹结构](https://www.lianxh.cn/news/f5fd1d23f1131.html)
  - [Stata数据处理：ascol-mtoq-日收益转周-月-季-年度数据](https://www.lianxh.cn/news/6949be099c315.html)
  - [Stata数据处理：import-fred-命令导入联邦储备经济数据库-FRED](https://www.lianxh.cn/news/e5d916ccf8d52.html)
  - [Stata 数据处理：nrow + labone 的巧妙使用](https://www.lianxh.cn/news/fa330690ab8fe.html)
  - [Stata数据处理：面板数据的填充和补漏](https://www.lianxh.cn/news/c2febe0f3530a.html)
  - [Stata: 约翰霍普金斯大学 COVID-19 疫情数据处理及可视化](https://www.lianxh.cn/news/ca5082adbf97f.html)
- 专题：[面板数据](https://www.lianxh.cn/blogs/20.html)
  - [Stata数据处理：xtbalance-非平衡面板之转换](https://www.lianxh.cn/news/0146f8d3b2861.html)
  - [Stata数据处理：如何提取某个变量有记录的第一年的年份](https://www.lianxh.cn/news/614e7d042b369.html)
- 专题：[Python-R-Matlab](https://www.lianxh.cn/blogs/37.html)
  - [Python 调用 API 爬取百度 POI 数据小贴士——坐标转换、数据清洗与 ArcGIS 可视化](https://www.lianxh.cn/news/a72842993b22b.html)




![](https://fig-lianxh.oss-cn-shenzhen.aliyuncs.com/Lianxh_装饰黄线.png)

&emsp;

## 相关课程

>### 免费公开课  

- [直击面板数据模型](https://lianxh.duanshu.com/#/brief/course/7d1d3266e07d424dbeb3926170835b38) - 连玉君，时长：1小时40分钟，[课程主页](https://gitee.com/arlionn/PanelData)，[Bilibili 站版](https://www.bilibili.com/video/BV1oU4y187qY)
- [Stata 33 讲](http://lianxh-pc.duanshu.com/course/detail/b22b17ee02c24015ae759478697df2a0) - 连玉君, 每讲 15 分钟. [课程主页](https://gitee.com/arlionn/stata101)，[课件](https://gitee.com/arlionn/stata101)，[Bilibili 站版](https://space.bilibili.com/546535876/channel/detail?cid=160748)    
- [Stata 小白的取经之路](https://lianxh.duanshu.com/#/brief/course/137d1b7c7c0045e682d3cf0cb2711530) - 龙志能，时长：2 小时，[课程主页](https://gitee.com/arlionn/StataBin) 
- 部分直播课 [课程资料下载](https://gitee.com/arlionn/Live) (PPT，dofiles等) 




>### 最新课程-直播课   

| 专题 | 嘉宾    | 直播/回看视频    |
| --- | --- | --- |
| &#x2B50; **[最新专题](https://www.lianxh.cn/news/46917f1076104.html)**|   | 文本分析、机器学习、效率专题、生存分析等 |
| 研究设计 | 连玉君    | [我的特斯拉-实证研究设计](https://lianxh.duanshu.com/#/course/5ae82756cc1b478c872a63cbca4f0a5e)，[-幻灯片-](https://gitee.com/arlionn/Live/tree/master/%E6%88%91%E7%9A%84%E7%89%B9%E6%96%AF%E6%8B%89-%E5%AE%9E%E8%AF%81%E7%A0%94%E7%A9%B6%E8%AE%BE%E8%AE%A1-%E8%BF%9E%E7%8E%89%E5%90%9B)|
| 面板模型 | 连玉君    | [动态面板模型](https://efves.duanshu.com/#/brief/course/3c3ac06108594577a6e3112323d93f3e)，[-幻灯片-](https://quqi.gblhgk.com/s/880197/o7tDK5tHd0YOlYJl)   |
| 面板模型 | 连玉君  | [直击面板数据模型](https://lianxh.duanshu.com/#/brief/course/7d1d3266e07d424dbeb3926170835b38) [免费公开课，2小时]  |

- Note: 部分课程的资料，PPT 等可以前往 [连享会-直播课](https://gitee.com/arlionn/Live) 主页查看，下载。

&emsp; 

> #### &#x26F3; [课程主页](https://www.lianxh.cn/news/46917f1076104.html)

[![](https://fig-lianxh.oss-cn-shenzhen.aliyuncs.com/lianxhbottom01.png)](https://www.lianxh.cn/news/46917f1076104.html)

[![](https://fig-lianxh.oss-cn-shenzhen.aliyuncs.com/lianxhbottom02.png)](https://www.lianxh.cn/news/46917f1076104.html)

[![](https://fig-lianxh.oss-cn-shenzhen.aliyuncs.com/lianxhbottom03.png)](https://www.lianxh.cn/news/46917f1076104.html)

> #### &#x26F3; [课程主页](https://www.lianxh.cn/news/46917f1076104.html)

&emsp;

>### 关于我们

- **Stata连享会** 由中山大学连玉君老师团队创办，定期分享实证分析经验。
- [连享会-主页](https://www.lianxh.cn) 和 [知乎专栏](https://www.zhihu.com/people/arlionn/)，400+ 推文，实证分析不再抓狂。[直播间](http://lianxh.duanshu.com) 有很多视频课程，可以随时观看。
- **公众号关键词搜索/回复** 功能已经上线。大家可以在公众号左下角点击键盘图标，输入简要关键词，以便快速呈现历史推文，获取工具软件和数据下载。常见关键词：`课程, 直播, 视频, 客服, 模型设定, 研究设计, stata, plus, 绘图, 编程, 面板, 论文重现, 可视化, RDD, DID, PSM, 合成控制法` 等

&emsp; 

> 连享会小程序：扫一扫，看推文，看视频……

[![](https://fig-lianxh.oss-cn-shenzhen.aliyuncs.com/连享会小程序二维码180.png)](https://www.lianxh.cn/news/46917f1076104.html)

&emsp; 

> 扫码加入连享会微信群，提问交流更方便

![](https://fig-lianxh.oss-cn-shenzhen.aliyuncs.com/连享会-学习交流微信群001-150.jpg)

> &#x270F;  连享会-常见问题解答：  
> &#x2728;  <https://gitee.com/lianxh/Course/wikis>  

&emsp; 

> <font color=red>New！</font> **`lianxh` 命令发布了：**    
> 随时搜索连享会推文、Stata 资源，安装命令如下：  
> &emsp; `. ssc install lianxh`  
> 使用详情参见帮助文件 (有惊喜)：   
> &emsp; `. help lianxh`

> 连享会 SSC 新命令：`cnssc`, `ihelp`, `rdbalance`, `readwind`, `gitee`, `installpkg`   
> 安装命令：`ssc install pkgname`   

> 重磅！——用 **cnssc** 极速安装外部命令   
> `cnssc`：Stata 官方命令 `ssc` 的中文镜像命令  
> &emsp; . `ssc install cnssc`   
> &emsp; . `cnssc install winsor2, replace`    
