/* log,记录结果窗口 */
cap log close
log using $logfiles\Stata连享会_CHFS数据清洗.log, replace
/* ======================================================
 * Program: Stata连享会_CHFS数据处理
 * Data:	CHFS 2015 2017 
 * Aim:   	Have fun with Stata 
 * Revised: 06/03/2021
 ======================================================== */

clear
global root= "E:\CHFS"       //在 E 盘自动创建 CHFS 研究题目，并在其下创建 9 个子文件夹，可以直接修改路径创建研究话题
global dofiles= "$root\Dofiles"
global logfiles= "$root\Logfiles"           
global raw_data= "$root\Raw_data"
global working_data= "$root\Working_data"
global temp_data= "$root\Temp_data"
global tables= "$root\Tables"
global figures= "$root\Figures"
cap !mkdir "$raw_data"              
cap mkdir "$temp_data"                //自动创建文件夹
cap mkdir "$working_data"
cap mkdir "$tables"                   // `cap` 命令可让错误的代码继续运行
cap mkdir "$figures"
cap mkdir "$dofiles"                  //如果已经创建了这些文件夹，也可以运行
cap mkdir "$logfiles"
cap mkdir "$root\Paper"
cap mkdir "$root\References"
cap mkdir "$raw_data"
cap mkdir  "$working_data"
cap mkdir "$figures"                     //以上命令在每次打开 do 文件都可运行
 
/* use,导入原始数据 */ 
set matsize 5000
set more off
use $raw_data\CHFS数据-2015\2015年中国家庭金融调查数据dta格式-stata14以上版本\chfs2015_hh_20191120_version14.dta,clear

describe   //描述所有数据
browse              //浏览数据
edit       //编辑数据 
preserve               //与restore一起使用可以恢复数据
drop _all
restore
preserve
rename _all,upper     //所有变量大写
rename _all,lower     //所有变量小写
rename _all,proper    //所有变量名首字母大写
rename * *_2021         //所有变量名后加相同后缀前缀
rename (*_2021 ) (havefun_*)  //批量修改变量名的前后缀
restore
gen family_size=a2000a+a2000b
label var family_size "家庭规模"            //为变量增加标签
sort  family_size                     //对数据进行排序
order  hhid family_size            //对变量进行排序

duplicates list hhid    //查询有无重复家户
duplicates drop hhid,force   //删除重复家户
gen year=2015           //产生新变量，生成年份 
egen incomemedian=median(total_income)  //产生新变量，去年总收入的中位数
egen incomemax=max(total_income)   //产生新变量，去年总收入的最大值
encode hhid, gen(newhhid)      //变量格式转换
gen nor=1 if d1101==1 | d2101==1 
recode nor(1=1)(.=0),gen(no_risk)   //变量值替换并产生新变量，无风险资产
recode a4011c (1=5)(2=4)(3=3)(4=2)(5=1),gen(happiness)   //产生幸福感变量
drop if happiness==. | happiness==.d | happiness==.r  //删除无效值
replace incomemax=incomemax/10       //把 incomemax 的值缩小 10 倍
drop if track==0                       //保留追访家户
keep if  track!=0                    //保留追访家户
bys hhid: egen mean_tolincome=mean(total_income)     //分组求平均  
bys hhid: egen newvar=sum(total_income)     //计算分组后的累计和  
bys hhid year: egen cc=max(total_income)      //计算分组后求最大最小值
recode c1001 (1=1)(2=2)(3=2) ,gen(hous)       
gen nf=c7001+hous
recode nf(2/3=1)(4=0),gen(no_fin)  //非金融资产
recode hous (1=1)(2=0) ,gen(house)  //房产
recode c7001(1=1)(2=0),gen(car)  //汽车
keep hhid  family_size   year  happiness total_income     //保留需要研究的变量
save $working_data\chfs_2015_hh.dta, replace    //保存数据

*相同变量数据文件合并，即跨年的面板数据合并
use $raw_data\CHFS数据-2017\CHFS2017年调查数据-stata14版本\chfs2017_hh_202104.dta, clear 
rename a1111 family_size  
ren b2003b total_income 
recode h3514 (1=5)(2=4)(3=3)(4=2)(5=1),gen(happiness)   //产生幸福感变量
gen year=2017
keep hhid    family_size   year  total_income happiness  //保留需要研究的变量
append using $working_data\chfs_2015_hh.dta   //相同变量数据文件合并
save $working_data\chfs_2015_2017_hh.dta, replace    //保存数据

*在原数据中加入新变量合并
/* use,导入原始数据 */ 
use $raw_data\CHFS数据-2015\2015年中国家庭金融调查数据dta格式-stata14以上版本\chfs2015_master_city_20180504_version14, clear 
/* merge,加入新变量合并数据 */ 
merge m:1 hhid using $working_data\chfs_2015_hh.dta //合并hh数据和master数据
keep if _merge==3                                     //保留匹配后所需数据
drop _merge                                         //需删除，若后续再次匹配则还会生成该变量
save $working_data\chfs_hh_master_2015_hh.dta, replace    //保存数据

use $raw_data\CHFS数据-2015\2015年中国家庭金融调查数据dta格式-stata14以上版本\chfs2015_ind_20191120_version14.dta,clear  //导入数据
recode a2003 (1=1)(2=0),gen(gender)        //性别
gen age=2021-a2005                         //年龄
rename a2012 edu                           //教育程度
recode a3000(1=1)(2=0),gen(work)           //有无工作
rename a2025b health           //健康程度
rename a2024 marital            //婚姻状况
gen income=a3020+a3022+a3023-a3024    //收入
recode a3014a(1/4=1)(5/7=0)(8=1)(9/10=0),gen(department)  //是否国企
tab gender,m            //性别分布
tab edu,m               //教育程度分布
sum edu,d               //平均值与方差
sum edu if gender==1    //男性受教育程度
sum edu if gender==0    //女性受教育程度
centile edu if  gender==1  ,centile(10(10)90)  //计算男性样本中 10% ， 20% ，...， 90% 分位数

*定性——定性变量描述统计
drop if gender==.
tab gender edu,row chi2             //不同性别的受教育程度分布差异，并卡方检验
by gender,sort:tab edu department,row   //不同性别、教育程度在工作单位性质的区别

*定性——定量变量描述统计
tabstat income,by(gender) statistics(mean sd p50 max min)   //不同性别收入
tabstat income,by(edu) statistics(mean sd)  //不同教育程度的平均收入
log close                //关闭 log 文件